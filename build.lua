----------------------------------------------------------- 
-- Build-file: build.lua
-- Copyright © 2022, 2023 निरंजन
--
-- This program is free software: you can redistribute it
-- and/or modify it under the terms of the GNU General
-- Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your
-- option) any later version.
--
-- This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the
-- implied warranty of MERCHANTABILITY or FITNESS FOR A
-- PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program. If not, see
-- <https://www.gnu.org/licenses/>.
-----------------------------------------------------------
module     = "gfdl"

-- Documentation:
typesetexe   = "lualatex"
typesetruns  = 2
installfiles = { "*.tex", "*.sty" }
sourcefiles  = { "*.ins","*.tex","*.dtx" }
typesetfiles = { "gfdl.dtx" }
docfiles     = { "COPYING","README.txt" }